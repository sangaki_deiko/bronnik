import {Component, NgModule, OnInit} from '@angular/core';
import {Router, RouterModule} from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { routerConfig } from '../top-menu.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: []
})

export class HomeComponent {

  constructor(private router: Router) {}

  ngOnInit(): void {

  }
}
