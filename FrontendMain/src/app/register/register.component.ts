import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { RegisterService } from './register.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  constructor(private service: RegisterService, private router: Router) { }

  ngOnInit(): void {
  }

  postNewClient(newClient: NgForm) {
    let birthdate = newClient.controls['birthdate'].value;
    let bdv = new Date(birthdate.year, birthdate.month-1, birthdate.day);
    let client = newClient.value;
    client['birthdate'] = bdv;
    this.service.createClient(client).subscribe(
      response => {
        console.log(response);
        this.router.navigate(['/success-register']);
      }, err => {
        console.log(err);
      });
  }
}
