import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routerConfig } from './top-menu.service';


@NgModule({
  imports: [RouterModule.forRoot(routerConfig)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
