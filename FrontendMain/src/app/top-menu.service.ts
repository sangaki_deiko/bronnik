import {HomeComponent} from './home/home.component';
import {Routes} from '@angular/router';
import {OrdersComponent} from './orders/orders.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './guards/auth-guard.service';
import {RegisterComponent} from './register/register.component';
import {NewsComponent} from './news/news.component';
import {NewsListComponent} from './news-list/news-list.component';
import {FeedbacksComponent} from './feedbacks/feedbacks.component';
import {LeaveFeedbackComponent} from './leave-feedback/leave-feedback.component';
import {RoomsComponent} from './rooms/rooms.component';
import {RoomSingleComponent} from './room-single/room-single.component';
import {NewOrderComponent} from './new-order/new-order.component';
import {SuccessRegisterComponent} from './success-register/success-register.component';

export const routerConfig: Routes = [
  {
    path: 'orders',
    component: OrdersComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'home',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: 'register',
    component: RegisterComponent,
    pathMatch: 'full'
  },
  {
    path: 'success-register',
    component: SuccessRegisterComponent,
    pathMatch: 'full'
  },
  {
    path: 'news',
    component: NewsComponent,
    pathMatch: 'full'
  },
  {
    path: 'news-list',
    component: NewsListComponent,
    pathMatch: 'full'
  },
  {
    path: 'feedbacks',
    component: FeedbacksComponent,
    pathMatch: 'full'
  },
  {
    path: 'leave-feedback',
    component: LeaveFeedbackComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'rooms',
    component: RoomsComponent,
    pathMatch: 'full'
  },
  {
    path: 'room-single',
    component: RoomSingleComponent,
    pathMatch: 'full'
  },
  {
    path: 'new-order',
    component: NewOrderComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
];
