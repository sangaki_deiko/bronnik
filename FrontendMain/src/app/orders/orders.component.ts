import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {OrdersService} from './orders.service';
import {Order} from '../classes/order';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styles: [``],
  providers: [OrdersService]
})
export class OrdersComponent implements OnInit  {
  @ViewChild('readOnlyTemplate', {static: false}) readOnlyTemplate: TemplateRef<any>;
  @ViewChild('newReserve', {static: false}) newReserve: TemplateRef<any>;

  orders: Array<Order>;
  editedOrder: Order;
  isNewRecord: boolean;

  constructor(private service: OrdersService, private modalService: NgbModal, private http: HttpClient, private router: Router) {
    this.orders = new Array<Order>();
  }

  closeResult = '';

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', backdrop: 'static', keyboard: false, size: 'lg'})
      .result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  loadTemplate(order: Order) {
    return this.readOnlyTemplate;
  }

  ngOnInit() {
    this.loadOrders();
  }

  loadOrders() {
    this.service.getOrders().subscribe((data: Order[]) => {
      this.orders = data;
    });
  }

  postNewOrder(AdminNewOrder: NgForm) {
    let birthdate = AdminNewOrder.controls['birthdate'].value;
    let bdv = new Date(birthdate.year, birthdate.month-1, birthdate.day);
    let date_in = AdminNewOrder.controls['date_in'].value;
    let dinv = new Date(date_in.year, date_in.month-1, date_in.day);
    let date_out = AdminNewOrder.controls['date_out'].value;
    let doutv = new Date(date_out.year, date_out.month-1, date_out.day);
    let order = AdminNewOrder.value;
    order['birthdate'] = bdv;
    order['date_in'] = dinv;
    order['date_out'] = doutv;
    order['id_room'] = parseInt(AdminNewOrder.controls['id_room'].value);
    console.log(order);
    this.service.createOrder(order).subscribe(
      response => {
      console.log(response);
    }, err => {
      console.log(err);
    });
  }

  moveToArchive(id: string) {
    let idn = parseInt(id);
    this.service.moveOrderArchive(idn).subscribe(
      response => {
      console.log(response);
    }, err => {
      console.log(err);
    });
  }

  openArchive() {
    this.router.navigate(['orders/archive']);
  }
}
