import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  private url = 'http://localhost:5050/api/news';
  constructor(private http: HttpClient){ }

  getNews(){
    return this.http.get(this.url);
  }
}
