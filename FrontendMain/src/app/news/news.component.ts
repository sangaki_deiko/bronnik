import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {News} from '../classes/news';
import {Order} from '../classes/order';
import {NewsService} from './news.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass']
})

export class NewsComponent implements OnInit {
  @ViewChild('newsTemplate', {static: false}) newsTemplate: TemplateRef<any>;

  News: Array<News>;

  constructor(private service: NewsService, private http: HttpClient, private router: Router) {
    this.News = new Array<News>();
  }

  loadTemplate(news: News) {
    return this.newsTemplate;
  }

  loadNews() {
    this.service.getNews().subscribe((data: News[]) => {
      this.News = data;
    });
  }

  ngOnInit(): void {
    this.loadNews();
  }

}
