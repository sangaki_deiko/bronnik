import { AuthGuard } from './guards/auth-guard.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { OrdersComponent } from './orders/orders.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { routerConfig } from './top-menu.service';
import { NewsComponent } from './news/news.component';
import { NewsListComponent } from './news-list/news-list.component';
import { FeedbacksComponent } from './feedbacks/feedbacks.component';
import { LeaveFeedbackComponent } from './leave-feedback/leave-feedback.component';
import { RegisterComponent } from './register/register.component';
import { RoomsComponent } from './rooms/rooms.component';
import { RoomSingleComponent } from './room-single/room-single.component';
import { NewOrderComponent } from './new-order/new-order.component';
import { SuccessRegisterComponent } from './success-register/success-register.component';

// @ts-ignore
@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent,
    AppComponent,
    OrdersComponent,
    NewsComponent,
    NewsListComponent,
    FeedbacksComponent,
    LeaveFeedbackComponent,
    RegisterComponent,
    RoomsComponent,
    RoomSingleComponent,
    NewOrderComponent,
    SuccessRegisterComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routerConfig),
    JwtModule.forRoot({
      config: {
        headerName: 'Authorization',
        tokenGetter: function tokenGetter() {
          return localStorage.getItem('jwt');
        },
        whitelistedDomains: ['localhost:5050'],
        blacklistedRoutes: []
      }
    }),
    NgbModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
