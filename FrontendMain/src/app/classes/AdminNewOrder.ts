export class AdminNewOrder{
  constructor(
    public Id_room: number,
    public Id_employee: number,
    public date_in: string,
    public date_out: string,
    public date_order: string,
    public total_price: number,
    public firstname: string,
    public lastname: string,
    public birthdate: string,
    public phone: string,
    public email: string,
    public passport: string) { }
}
