﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bronnik.Contexts;
using bronnik.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace bronnik.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatsController : ControllerBase
    {
        private readonly ApplicationContext _context;

        public StatsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: api/Stats/orders
        [HttpGet]
        [Route("/orders")]
        public async Task<ActionResult<IEnumerable<Order>>> GetOrdersStats()
        {
            return await _context.Orders.ToListAsync();
        }

        // GET: api/Stats/clients
        [HttpGet]
        [Route("/clients")]
        public async Task<ActionResult<IEnumerable<Client>>> GetClientsStats()
        {
            return await _context.Clients.ToListAsync();
        }

        // GET: api/Stats/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
    }
}
