﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using bronnik.Contexts;
using bronnik.Models;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using bronnik.Options;
using Microsoft.IdentityModel.Tokens;
using bronnik.Models.ActionModels;

namespace bronnik.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersAuthController : ControllerBase
    {
        private readonly ApplicationContext _context;

        public UsersAuthController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutClient(int id, Client client)
        {
            if (id != client.Id_client)
            {
                return BadRequest();
            }

            _context.Entry(client).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost, Route("register")]
        public async Task<ActionResult<Client>> PostClient(Client client)
        {
            if (EmailUsed(client.email))
            {
                return BadRequest("EMAIL_ALREADY_USED");
            }

            _context.Clients.Add(client);
            await _context.SaveChangesAsync();
            
            return CreatedAtAction("GetClient", new { id = client.Id_client }, client);
        }

        [HttpGet]
        public async Task<ActionResult<Client>> GetClient(string Id)
        {
            var client = await _context.Clients.FindAsync(Id);

            if (client == null)
            {
                return NotFound();
            }

            return client;
        }

        [HttpPost, Route("login")]
        public IActionResult Login([FromBody]LoginClientModel client)
        {
            if (client == null)
            {
                return BadRequest("Invalid client request");
            }

            var identity = GetIdentity(client.email, client.password);

            if (identity == null)
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                token = encodedJwt,
                id = 0
            };

            return new JsonResult(response);
        }

        private ClaimsIdentity GetIdentity(string email, string password)
        {
            Client person = _context.Clients.FirstOrDefault(x => x.email == email && x.password == password);
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.email),
                    new Claim("firstname", person.firstname)
                };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }


        private bool EmailUsed(string email)
        {
            return _context.Clients.Any(e => e.email == email);
        }

        private bool ClientExists(int id)
        {
            return _context.Clients.Any(e => e.Id_client == id);
        }
    }
}
