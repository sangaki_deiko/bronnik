﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using bronnik.Contexts;
using bronnik.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using bronnik.Models.ActionModels;
using Newtonsoft.Json;

namespace bronnik.Controllers
{
    [EnableCors("MyCors")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly ApplicationContext _context;

        public OrdersController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: api/Orders
        [HttpGet]
        public ActionResult<IEnumerable<OrdersList>> GetOrders()
        {
            var listOrders = _context.Orders
                .Where(p => p.archived != true)
                .Select(c => new OrdersList
                {
                    Id = c.Id_order,
                    firstname = c.Client.firstname,
                    lastname = c.Client.lastname,
                    Id_room = c.Id_room,
                    date_in = c.date_in.ToString("dd.MM.yyyy"),
                    date_out = c.date_out.ToString("dd.MM.yyyy"),
                    date_order = c.date_order.ToString("dd.MM.yyyy"),
                    total_price = c.total_price
                })
                .AsEnumerable()
                .ToList();
            return listOrders;
        }

        // GET: api/Orders/Archive
        [HttpGet]
        [Route("archive")]
        public ActionResult<IEnumerable<OrdersList>> GetOrdersArchive()
        {
            var listOrders = _context.Orders
                .Where(p => p.archived == true)
                .Select(c => new OrdersList
                {
                    Id = c.Id_order,
                    firstname = c.Client.firstname,
                    lastname = c.Client.lastname,
                    Id_room = c.Id_room,
                    date_in = c.date_in.ToString("dd.MM.yyyy"),
                    date_out = c.date_out.ToString("dd.MM.yyyy"),
                    date_order = c.date_order.ToString("dd.MM.yyyy"),
                    total_price = c.total_price
                })
                .AsEnumerable()
                .ToList();
            return listOrders;
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetOrder(int id)
        {
            var order = await _context.Orders.FindAsync(id);

            if (order == null)
            {
                return NotFound();
            }

            return order;
        }

        // PUT: api/Orders/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder(int id, Order order)
        {
            if (id != order.Id_order)
            {
                return NotFound();
            }

            _context.Entry(order).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Orders
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Order>> PostOrder(AdminNewOrder newOrder)
        {
            Console.WriteLine(newOrder);
            var birthdate = DateTime.Parse(newOrder.birthdate);
            var date_in = DateTime.Parse(newOrder.date_in);
            var date_out = DateTime.Parse(newOrder.date_out);
            Client client = new Client 
            {
                firstname = newOrder.firstname,
                lastname = newOrder.lastname,
                birthdate = birthdate,
                passport = newOrder.passport,
                phone = newOrder.phone,
                email = newOrder.email

            };
            Order order = new Order
            {
                Id_room = newOrder.id_room,
                date_in = date_in,
                date_out = date_out,
                date_order = DateTime.Now,
                Client = client
            };
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOrder", new { id = order.Id_order }, order);
        }

        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Order>> DeleteOrder(int id)
        {
            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();

            return order;
        }

        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.Id_order == id);
        }
    }
}
