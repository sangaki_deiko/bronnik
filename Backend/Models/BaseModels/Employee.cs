﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models
{
    public class Employee
    {
        [Key]
        public int Id_employee { get; set; }
        public int Id_position { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd/mm/yyyy}")]
        public DateTime birthdate { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string passport { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public virtual Positions Positions { get; set; }
    }
}
