﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models
{
    public class RoomCategory
    {
        [Key]
        public int Id_category { get; set; }
        public string category_name { get; set; }
        public int number_of_beds { get; set; }
        public int price_per_day { get; set; }
    }
}
