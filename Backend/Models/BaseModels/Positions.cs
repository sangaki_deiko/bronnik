﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models
{
    public class Positions
    {
        [Key]
        public int Id_position { get; set; }
        public string description { get; set; }
        public int day_remuneration { get; set; }
    }
}
