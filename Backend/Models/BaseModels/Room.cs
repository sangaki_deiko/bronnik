﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models
{
    public class Room
    {
        [Key]
        public int Id_room { get; set; }
        public int Id_category { get; set; }
        public string photo { get; set; }
        public string description { get; set; }
        public virtual RoomCategory RoomCategory { get; set; }
    }
}
