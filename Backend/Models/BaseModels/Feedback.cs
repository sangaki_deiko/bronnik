﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models
{
    public class Feedback
    {
        [Key]
        public int Id_feedback { get; set; }
        public int Id_client { get; set; }
        public string title { get; set; }
        public string text { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd/mm/yyyy}")]
        public DateTime date { get; set; }
        public int value { get; set; }


        public virtual Client Client { get; set; }
    }
}
