﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models
{
    public class News
    {
        [Key]
        public int Id_news { get; set; }
        public int Id_employee { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string photo { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd/mm/yyyy}")]
        public DateTime date { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
