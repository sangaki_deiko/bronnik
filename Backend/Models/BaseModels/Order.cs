﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models
{
    public class Order
    {
        [Key]
        public int Id_order { get; set; }
        public int Id_client { get; set; }
        public int Id_room { get; set; }
        public int Id_employee { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd/mm/yyyy}")]
        public DateTime date_in { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd/mm/yyyy}")]
        public DateTime date_out { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd/mm/yyyy}")]
        public DateTime date_order { get; set; }
        public int total_price { get; set; }
        public bool archived { get; set; }

        public virtual Client Client { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Room Room { get; set; }

    }

    public class AdminNewOrder
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string birthdate { get; set; }
        public string passport { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public int id_room { get; set; }
        public string date_in { get; set; }
        public string date_out { get; set; }

        public virtual Client Client { get; set; }
    }

    public class RequestDate
    {
        public int year { get; set; }
        public int month { get; set; }
        public int day { get; set; }
    }
}
