﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models
{
    public class Client
    {
        [Key]
        public int Id_client { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd/mm/yyyy}")]
        public DateTime birthdate { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string passport { get; set; }
        public string password { get; set; }
    }
}
