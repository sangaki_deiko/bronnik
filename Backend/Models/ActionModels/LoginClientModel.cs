﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models.ActionModels
{
    public class LoginClientModel
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
