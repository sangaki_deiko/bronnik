﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bronnik.Models.ActionModels
{
    public class OrdersList
    {
        public int Id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public int Id_room { get; set; }
        public string date_in { get; set; }
        public string date_out { get; set; }
        public string date_order { get; set; }
        public int total_price { get; set; }
    }
}
