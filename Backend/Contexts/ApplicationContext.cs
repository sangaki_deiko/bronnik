﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bronnik.Models;
using Microsoft.EntityFrameworkCore;

namespace bronnik.Contexts
{

    public class ApplicationContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Positions> Positions { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomCategory> RoomCategories { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<News> News { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=192.168.0.62;Port=5432;Database=postgres;Username=postgres;Password=qwerty@1");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string adminRoleName = "admin";

            string adminEmail = "poirtain@mail.ru";
            string adminLogin = "Admin";
            string adminPassword = "qwerty@1";

            // добавляем роли
            Positions adminRole = new Positions { Id_position = 1, description = "Admin", day_remuneration = 90000 };
            Employee adminUser = new Employee { Id_employee = 1, Id_position = adminRole.Id_position, birthdate = new DateTime(1996, 10, 26), firstname = "Михаил", lastname = "Ворожба", 
            email = adminEmail, passport = "4016 123456", phone = "+79995251848", login = adminLogin, password = adminPassword };

            modelBuilder.Entity<Positions>().HasData(new Positions[] { adminRole });
            modelBuilder.Entity<Employee>().HasData(new Employee[] { adminUser });
            base.OnModelCreating(modelBuilder);
        }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
    }
}
