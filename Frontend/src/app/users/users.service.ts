import { Injectable } from '@angular/core';
import {Order} from '../classes/order';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient){ }

  private url = 'http://192.168.0.62:5050/api/Employees';

  getEmployees(){
    return this.http.get(this.url);
  }

  addEmployee(order: Order){
    const myHeaders = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(this.url, JSON.stringify(order), {headers: myHeaders});
  }
}
