import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Employee} from '../classes/employee';
import {UsersService} from './users.service';
import {Order} from '../classes/order';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass']
})
export class UsersComponent implements OnInit {
  @ViewChild('employeesTemplate', {static: false}) employeesTemplate: TemplateRef<any>;

  employees: Array<Employee>;

  constructor(private service: UsersService, private modalService: NgbModal, private http: HttpClient, private router: Router) {
    this.employees = new Array<Employee>();
  }

  ngOnInit(): void {
    this.loadEmployees();
  }

  loadEmployees() {
    this.service.getEmployees().subscribe((data: Employee[]) => {
      this.employees = data;
    });
  }

  closeResult = '';

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', backdrop: 'static', keyboard: false, size: 'lg'})
      .result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  loadTemplate(employee: Employee) {
    return this.employeesTemplate;
  }

  postNewEmployee(AdminNewEmployee: NgForm) {
    let birthdate = AdminNewEmployee.controls['birthdate'].value;
    let bdv = new Date(birthdate.year, birthdate.month-1, birthdate.day);
    let order = AdminNewEmployee.value;
    order['birthdate'] = bdv;
    order['id_position'] = parseInt(AdminNewEmployee.controls['id_position'].value);
    console.log(order);
    this.service.addEmployee(order).subscribe(
      response => {
        console.log(response);
      }, err => {
        console.log(err);
      });
  }
}
