import { AuthGuard } from './guards/auth-guard.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { OrdersComponent } from './orders/orders.component';
import { ArchiveOrdersComponent} from './archive-orders/archive-orders.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {routerConfig} from './top-menu.service';
import { UsersComponent } from './users/users.component';
import { AnalyticComponent } from './analytic/analytic.component';
import { ChartistModule } from 'ng-chartist';
import { NewsComponent } from './news/news.component';
import { FeedbacksComponent } from './feedbacks/feedbacks.component';

// @ts-ignore
@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent,
    AppComponent,
    OrdersComponent,
    ArchiveOrdersComponent,
    UsersComponent,
    AnalyticComponent,
    NewsComponent,
    FeedbacksComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routerConfig),
    ChartistModule,
    JwtModule.forRoot({
      config: {
        headerName: 'Authorization',
        tokenGetter: function tokenGetter() {
          return localStorage.getItem('jwt');
        },
        whitelistedDomains: ['localhost:5050'],
        blacklistedRoutes: []
      }
    }),
    NgbModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
