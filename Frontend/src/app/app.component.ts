import { Component, OnInit } from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import { routerConfig } from './top-menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: []
})

export class AppComponent implements OnInit {
  title: 'BronnieApp';
  userDisplayName = '';

  constructor(private jwtHelper: JwtHelperService, private router: Router) {}

  isUserAuthenticated() {
    const token: string = localStorage.getItem('jwt');
    return token && !this.jwtHelper.isTokenExpired(token);
  }

  logOut() {
    localStorage.removeItem('jwt');
    localStorage.removeItem('username');
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
    this.userDisplayName = localStorage.getItem('username');
  }

  updateName() {
    this.userDisplayName = localStorage.getItem('username');
  }
}
