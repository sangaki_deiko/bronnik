import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AppComponent } from '../app.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
  invalidLogin: boolean;

  constructor(private router: Router, private http: HttpClient, private rt: AppComponent) { }

  login(form: NgForm) {
    const credentials = JSON.stringify(form.value);
    this.http.post('http://192.168.0.62:5050/api/auth/login', credentials, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).subscribe(response => {
      const token = (response as any).token;
      const username = (response as any).username;
      localStorage.setItem('jwt', token);
      localStorage.setItem('username', username);
      this.invalidLogin = false;
      this.router.navigate(['/home']);
      this.rt.updateName();
    }, err => {
      this.invalidLogin = true;
    });
  }
}
