export class AdminNewEmployee {
  constructor(
    public firstname: string,
    public lastname: string,
    public birthdate: string,
    public phone: string,
    public email: string,
    public passport: string,
    public id_position: string,
    public login: string,
    public password: string) { }
}
