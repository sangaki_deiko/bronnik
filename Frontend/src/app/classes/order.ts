export class Order{
  constructor(
    public Id: number,
    public ClientFirstName: string,
    public ClientLastName: string,
    public Room: number,
    public GoIn: string,
    public GoOut: string,
    public Created: string,
    public Total: number) { }
}
