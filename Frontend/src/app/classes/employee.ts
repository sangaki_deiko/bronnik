export class Employee{
  constructor(
    public Id: number,
    public EmployeeFirstName: string,
    public EmployeeLastName: string,
    public Position: number,
    public Birthdate: string) { }
}
