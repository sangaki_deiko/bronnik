import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Order} from '../classes/order';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  private url = 'http://192.168.0.62:5050/api/orders';
  private archive = 'http://192.168.0.62:5050/api/orders/archive';
  constructor(private http: HttpClient){ }

  getOrders(){
    return this.http.get(this.url);
  }

  getArchiveOrders(){
    return this.http.get(this.archive);
  }

  createOrder(order: Order){
    const myHeaders = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(this.url, JSON.stringify(order), {headers: myHeaders});
  }

  moveOrderArchive(id: number){
    const myHeaders = new HttpHeaders().set('Content-Type', 'application/json');
    let archId = {Id_order: id, archived: true};
    console.log(archId);
    return this.http.put(this.url + '/' + id, JSON.stringify(archId), {headers: myHeaders});
  }

  updateOrder(id: number, order: Order) {
    const myHeaders = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.put(this.url, JSON.stringify(order), {headers: myHeaders});
  }
  deleteOrder(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
