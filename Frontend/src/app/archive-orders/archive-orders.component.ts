import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Order} from '../classes/order';
import {OrdersService} from '../orders/orders.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-archive-orders',
  templateUrl: './archive-orders.component.html',
  styleUrls: ['./archive-orders.component.sass']
})
export class ArchiveOrdersComponent implements OnInit {
  @ViewChild('archiveOrder', {static: false}) archiveOrder: TemplateRef<any>;

  orders: Array<Order>;

  constructor(private service: OrdersService, private router: Router) {
    this.orders = new Array<Order>();
  }

  loadTemplate(order: Order) {
    return this.archiveOrder;
  }

  ngOnInit(): void {
    this.loadArchiveOrders();
  }

  loadArchiveOrders() {
    this.service.getArchiveOrders().subscribe((data: Order[]) => {
      this.orders = data;
    });
  }

  openOrders() {
    this.router.navigate(['orders']);
  }
}
