import {HomeComponent} from './home/home.component';
import {Routes} from '@angular/router';
import {OrdersComponent} from './orders/orders.component';
import {ArchiveOrdersComponent} from './archive-orders/archive-orders.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './guards/auth-guard.service';
import {UsersComponent} from './users/users.component';
import {AnalyticComponent} from './analytic/analytic.component';

export const routerConfig: Routes = [
  {
    path: 'orders',
    component: OrdersComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'analytic',
    component: AnalyticComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'users',
    component: UsersComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'orders/archive',
    component: ArchiveOrdersComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'home',
    component: HomeComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
];
